package net.convnet.server.util;

import java.util.HashMap;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-16
 */
public final class AttrMap extends HashMap<String, Object> {
    private static final long serialVersionUID = 52850833589542098L;

    public AttrMap() {
    }

    public AttrMap(String name, Object value) {
        super();
        put(name, value);
    }

    public AttrMap set(String name, Object value) {
        put(name, value);
        return this;
    }

    public AttrMap sets(HashMap<String, Object> attrs) {
        putAll(attrs);
        return this;
    }
}
