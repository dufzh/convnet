package net.convnet.server.util;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 11-12-26
 */
public final class ClassUtils {

    public static Class getGenericParameter(Class clazz, int index) {
        Type genType = clazz.getGenericSuperclass();
        if (genType instanceof ParameterizedType) {
            Type param = ((ParameterizedType) genType).getActualTypeArguments()[index];
            if (param instanceof Class) {
                return (Class) param;
            }
        }
        return null;
    }

    public static Class getGenericParameter0(Class clazz) {
        return getGenericParameter(clazz, 0);
    }

    public static <T> T newInstance(Class<T> clazz) {
        try {
            return clazz.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private ClassUtils() {
    }
}
