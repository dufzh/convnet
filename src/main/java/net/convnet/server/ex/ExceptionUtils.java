package net.convnet.server.ex;

import net.convnet.server.support.message.NLS;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.NestedRuntimeException;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashSet;
import java.util.Set;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-8-12
 */
public class ExceptionUtils {
    private ExceptionUtils() {
    }

    public static Throwable getRootCause(Throwable cause) {
        Throwable rootCause = null;
        while (cause != null && cause != rootCause) {
            rootCause = cause;
            cause = cause.getCause();
        }
        return rootCause;
    }

    public static String buildMessage(int code, Object[] args, String defaultMessage, Throwable cause) {
        String message = null;
        if (code != ErrorCode.SERVER_ERROR) {
            message = NLS.getMessage("error." + code, args, null);
        }
        if (StringUtils.isNotEmpty(defaultMessage)) {
            message = message == null ? defaultMessage : message + "; " + defaultMessage;
        }
        if (message == null) {
            message = "errorcode:" + code;
        }
        if (cause != null) {
            return buildMessage(message, cause);
        } else {
            return message;
        }
    }

    public static String buildMessage(String message, Throwable cause) {
        StringBuilder sb = new StringBuilder(message);
        Set<Throwable> visitedExceptions = new HashSet<Throwable>();
        Throwable tmpEx = cause;
        do {
            if (sb.length() > 0) {
                sb.append(" -> ");
            }
            sb.append(cause);
            visitedExceptions.add(tmpEx);
            tmpEx = tmpEx.getCause();
        }
        while (!(tmpEx == null || visitedExceptions.contains(tmpEx) || tmpEx instanceof ConvnetException || tmpEx instanceof NestedRuntimeException));
        return sb.toString();
    }


    public static String buildMessage(Throwable ex) {
        return buildMessage("", ex);
    }

    public static String buildStackTrace(Throwable cause) {
        StringWriter sw = new StringWriter();
        cause.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }
}
