package net.convnet.server.ex;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-7
 */
public class ConvnetException extends RuntimeException implements ErrorCode {
    private static final long serialVersionUID = 4595549517180869921L;
    private int code = SERVER_ERROR;
    private Object args[];

    public ConvnetException() {
        super();
    }

    public ConvnetException(String message) {
        super(message);
    }

    public ConvnetException(Throwable cause) {
        super(cause);
    }

    public ConvnetException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConvnetException(int code, Object... args) {
        super();
        this.code = code;
        this.args = args;
    }

    public ConvnetException(String defaultMessage, int code, Object... args) {
        super(defaultMessage);
        this.code = code;
        this.args = args;
    }

    public ConvnetException(Throwable cause, int code, Object... args) {
        super(cause);
        this.code = code;
        this.args = args;
    }

    public ConvnetException(String defaultMessage, Throwable cause, int code, Object... args) {
        super(defaultMessage, cause);
        this.code = code;
        this.args = args;
    }

    public int getCode() {
        return code;
    }

    public Object[] getArgs() {
        return args;
    }

    @Override
    public String getMessage() {
        return ExceptionUtils.buildMessage(code, args, super.getMessage(), getCause());
    }

    @Override
    public String toString() {
        return getMessage();
    }

    public static ConvnetException fromRoot(Exception e) {
        return new ConvnetException(ExceptionUtils.getRootCause(e));
    }
}
