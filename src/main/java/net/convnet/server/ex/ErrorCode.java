package net.convnet.server.ex;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 11-11-20
 */
public interface ErrorCode {
    /**
     * 自定义错误起始值
     */
    static final int CUSTOM = 100;

    /**
     * 请求成功执行
     */
    static final int SUCCEED = 0;

    /**
     * 内部错误
     */
    static final int SERVER_ERROR = 1;

    /**
     * 非法参数
     */
    static final int ILLEGAL_PARAM = 2;

    /**
     * 必须参数不存在
     */
    static final int MISS_PARAM = 3;

    /**
     * 状态错误
     */
    static final int ILLEGAL_STATE = 4;

    /**
     * 不支持此操作
     */
    static final int UN_SUPPORTED = 5;

    /**
     * 没有权限
     */
    static final int NO_PERMISSON = 6;

    /**
     * 实体对象错误
     */
    static final int ENTITY_ERROR = 50;

    /**
     * 实体对象未找到
     */
    static final int ENTITY_NOT_FOUND = 51;

    /**
     * 实体对象已存在
     */
    static final int ENTITY_EXISTS = 52;

    /**
     * 编码解码错误
     */
    static final int CODEC_ERROR = CUSTOM + 1;

    /**
     * 密码错误
     */
    static final int PASSWORD_INCORRECT = CUSTOM + 2;

}
