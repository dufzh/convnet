package net.convnet.server.support.hibernate;

import com.google.common.collect.Sets;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.cfg.Environment;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;

import javax.sql.DataSource;
import java.util.Properties;
import java.util.Set;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oznyang@163.com">oznyang</a>
 * @version V1.0, 14-1-20
 */
public class LocalSessionFactoryBean extends org.springframework.orm.hibernate4.LocalSessionFactoryBean {
    private static final Set<String> AUTOS = Sets.newHashSet("update", "create", "create-drop");
    private DatabasePopulator databasePopulator;

    public void setDatabasePopulator(DatabasePopulator databasePopulator) {
        this.databasePopulator = databasePopulator;
    }

    @Override
    protected SessionFactory buildSessionFactory(LocalSessionFactoryBuilder sfb) {
        SessionFactory sessionFactory = super.buildSessionFactory(sfb);
        populateDatabase();
        return sessionFactory;
    }

    private void populateDatabase() {
        Properties properties = getConfiguration().getProperties();
        String hbm2ddlAuto = properties.getProperty(AvailableSettings.HBM2DDL_AUTO);
        if (databasePopulator != null && hbm2ddlAuto != null && AUTOS.contains(hbm2ddlAuto)) {
            DatabasePopulatorUtils.execute(this.databasePopulator, (DataSource) properties.get(Environment.DATASOURCE));
        }
    }
}
