package net.convnet.server.support.hibernate;

import net.convnet.server.ex.EntityNotFoundException;
import net.convnet.server.util.ClassUtils;
import org.hibernate.*;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.internal.CriteriaImpl;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.*;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-6-20
 */
@SuppressWarnings("unchecked")
public class HibernateRepository<E, PK extends Serializable> {

    private final Class<E> entityClass;
    private SessionFactory sessionFactory;

    public HibernateRepository(Class<E> entityClass, SessionFactory sessionFactory) {
        this.entityClass = entityClass;
        this.sessionFactory = sessionFactory;
    }

    public HibernateRepository(SessionFactory sessionFactory) {
        this();
        this.sessionFactory = sessionFactory;
    }

    public HibernateRepository() {
        if (!AopUtils.isAopProxy(this)) {
            entityClass = ClassUtils.getGenericParameter0(getClass());
            if (entityClass == null) {
                throw new IllegalArgumentException("Entity class not found");
            }
        } else {
            entityClass = null;
        }
    }

    @Autowired
    @Qualifier("sessionFactory")
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public final Class<E> getEntityClass() {
        return entityClass;
    }

    public final SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public final Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public Criteria criteria(Criterion... criterions) {
        return criteria(Arrays.asList(criterions));
    }

    public Criteria criteria(Collection<Criterion> criterions) {
        Criteria criteria = getSession().createCriteria(entityClass);
        if (criterions != null) {
            for (Criterion c : criterions) {
                if (c != null) {
                    criteria.add(c);
                }
            }
        }
        return criteria;
    }

    public Query hql(String hql, Object... args) {
        Query query = getSession().createQuery(hql);
        return addParameters(query, args);
    }

    public Query hql(String hql, Map<String, Object> args) {
        Query query = getSession().createQuery(hql);
        return addParameters(query, args);
    }

    public E get(PK id) {
        return (E) getSession().get(entityClass, id);
    }

    public E load(PK id) throws EntityNotFoundException {
        return assertNotNull(get(id), id);
    }

    public boolean exists(PK id) {
        return get(id) != null;
    }

    public E getByNaturalId(String fieldName, Object fieldValue) {
        return getByNaturalId(Collections.singletonMap(fieldName, fieldValue));
    }

    public E getByNaturalId(Map<String, Object> naturalIds) {
        NaturalIdLoadAccess nia = getSession().byNaturalId(entityClass);
        for (Map.Entry<String, Object> entry : naturalIds.entrySet()) {
            nia.using(entry.getKey(), entry.getValue());
        }
        return (E) nia.load();
    }

    public E loadByNaturalId(String fieldName, Object fieldValue) {
        return loadByNaturalId(Collections.singletonMap(fieldName, fieldValue));
    }

    public E loadByNaturalId(Map<String, Object> naturalIds) {
        NaturalIdLoadAccess nia = getSession().byNaturalId(entityClass);
        for (Map.Entry<String, Object> entry : naturalIds.entrySet()) {
            nia.using(entry.getKey(), entry.getValue());
        }
        return assertNotNull((E) nia.load(), naturalIds);
    }

    public E get(Criteria criteria) {
        return (E) criteria.uniqueResult();
    }

    public E load(Criteria criteria) throws EntityNotFoundException {
        return assertNotNull(get(criteria), "");
    }

    public boolean exists(Criteria criteria) {
        return get(criteria) != null;
    }

    public E get(Query query) {
        return (E) query.uniqueResult();
    }

    public E load(Query query) throws EntityNotFoundException {
        return assertNotNull(get(query), "");
    }

    public boolean exists(Query query) {
        return get(query) != null;
    }

    public long count(Criteria criteria) {
        CriteriaImpl ci = (CriteriaImpl) criteria;
        Iterator<?> it = ci.iterateOrderings();
        while (it.hasNext()) {
            it.next();
            it.remove();
        }
        criteria.setFirstResult(0).setMaxResults(1);
        return Hibernates.getLong(criteria.setProjection(Projections.rowCount()).uniqueResult());
    }

    public List<E> list() {
        return list(criteria());
    }

    public List<E> list(Criteria criteria) {
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<E> list(Query query) {
        return query.list();
    }

    public Page<E> find(Criteria criteria, Pageable request) {
        if (request == null) {
            return new PageImpl<E>(list(criteria));
        }
        criteria.setFirstResult(request.getOffset()).setMaxResults(request.getPageSize());
        if (request.getSort() != null) {
            for (org.springframework.data.domain.Sort.Order order : request.getSort()) {
                if (order != null) {
                    criteria.addOrder(order.isAscending() ? org.hibernate.criterion.Order.asc(order.getProperty()) : org.hibernate.criterion.Order.desc(order.getProperty()));
                }
            }
        }
        return new PageImpl<E>(list(criteria), request, count(criteria));
    }

    public int update(Query query) {
        return query.executeUpdate();
    }

    public E save(E entity) {
        getSession().persist(entity);
        return entity;
    }

    public E merge(E entity) {
        getSession().merge(entity);
        return entity;
    }

    public void delete(E entity) {
        getSession().delete(entity);
    }

    public void deleteByPK(PK id) throws EntityNotFoundException {
        delete(load(id));
    }

    private E assertNotNull(E entity, Object arg) {
        if (entity == null) {
            throw new EntityNotFoundException(entityClass, arg.toString());
        }
        return entity;
    }

    private static <Q extends Query> Q addParameters(Q query, Object... args) {
        if (args != null) {
            for (int i = 0, len = args.length; i < len; i++) {
                query.setParameter(i, args[i]);
            }
        }
        return query;
    }

    private static <Q extends Query> Q addParameters(Q query, Map<String, Object> args) {
        if (args != null) {
            for (Map.Entry<String, Object> entry : args.entrySet()) {
                Object arg = entry.getValue();
                if (arg.getClass().isArray()) {
                    query.setParameterList(entry.getKey(), (Object[]) entry.getValue());
                } else if (arg instanceof Collection) {
                    query.setParameterList(entry.getKey(), ((Collection) arg));
                } else {
                    query.setParameter(entry.getKey(), arg);
                }
            }
        }
        return query;
    }
}
