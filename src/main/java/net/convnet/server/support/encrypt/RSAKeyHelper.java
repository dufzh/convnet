package net.convnet.server.support.encrypt;

import org.apache.commons.codec.binary.Base64;

import java.security.*;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-8-23
 */
public class RSAKeyHelper {
    public static void main(String[] args) throws Exception {
        KeyPairGenerator keygen = KeyPairGenerator.getInstance("RSA");
        SecureRandom random = new SecureRandom();
        if (args.length > 1) {
            random.setSeed(args[1].getBytes("utf-8"));
        } else {
            random.setSeed(random.nextLong());
        }
        keygen.initialize(1024, random);
        KeyPair kp = keygen.generateKeyPair();
        PublicKey publicKey = kp.getPublic();
        PrivateKey privateKey = kp.getPrivate();
        System.out.println("------------------------- publicKey ------------------------");
        System.out.println(Base64.encodeBase64String(publicKey.getEncoded()));
        System.out.println("------------------------- privateKey ------------------------");
        System.out.println(Base64.encodeBase64String(privateKey.getEncoded()));
        System.out.println("-------------------------------------------------------------");
    }
}
