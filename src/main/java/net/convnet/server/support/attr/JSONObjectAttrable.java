package net.convnet.server.support.attr;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import net.convnet.server.support.hibernate.JSONType;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-11-7
 */
@MappedSuperclass
public class JSONObjectAttrable extends AbstractAttrable implements Serializable {
    private static final long serialVersionUID = 460183492223957626L;
    @Type(type = JSONType.TYPE)
    @Column(length = 4000)
    private JSONObject attr;

    public JSONObjectAttrable(JSONObject attr) {
        this.attr = attr;
    }

    public JSONObjectAttrable(Map<String, Object> map) {
        this.attr = new JSONObject(map);
    }

    public JSONObjectAttrable() {
        attr = new JSONObject();
    }

    @JSONField(serialize = false)
    public JSONObject getAttr() {
        return attr;
    }

    public void setAttr(JSONObject attr) {
        this.attr = attr;
    }

    @Override
    public boolean hasAttr(String key) {
        return attr.containsKey(key);
    }

    @Override
    public <T> T getAttr(String key, Class<T> targetType) {
        Object obj = attr.get(key);
        return convert(obj, targetType);
    }

    @Override
    @JSONField(serialize = false)
    public String[] getKeys() {
        Collection<String> keys = attr.keySet();
        return keys.toArray(new String[keys.size()]);
    }

    @Override
    public void setAttr(String key, Object value) {
        attr.put(key, value);
    }

    @Override
    public void setAttrs(Map<String, ?> map) {
        attr.putAll(map);
    }

    @Override
    public void removeAttr(String key) {
        attr.remove(key);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        JSONObjectAttrable obj = (JSONObjectAttrable) super.clone();
        JSONObject json = new JSONObject();
        json.putAll(getAttr());
        obj.setAttr(json);
        return obj;
    }

    public static <T> T convert(Object value, Class<T> targetType) {
        return FastjsonConverter.INSTANCE.convert(value, targetType);
    }
}
