package net.convnet.server.support.attr;

import com.google.common.collect.Maps;
import org.springframework.util.CollectionUtils;
import org.springframework.util.PropertyPlaceholderHelper;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import static org.springframework.util.SystemPropertyUtils.*;

/**
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-9-26
 * @see org.springframework.core.env.AbstractPropertyResolver
 */
public abstract class AbstractAttrable implements Attrable {
    private static final PropertyPlaceholderHelper helper = new PropertyPlaceholderHelper(PLACEHOLDER_PREFIX, PLACEHOLDER_SUFFIX, VALUE_SEPARATOR, true);

    @Override
    public boolean hasAttr(String key) {
        return getAttr(key) != null;
    }

    @Override
    public String getAttr(String key) {
        return getAttr(key, String.class);
    }

    @Override
    public String getAttr(String key, String defaultValue) {
        String value = getAttr(key);
        return value == null ? defaultValue : value;
    }

    @Override
    public String getRequiredAttr(String key) throws IllegalStateException {
        String value = getAttr(key);
        if (value == null) {
            throw new IllegalStateException("required key [" + key + "] not found");
        }
        return value;
    }

    @Override
    public String resolvePlaceholders(String text) {
        return helper.replacePlaceholders(text, new PropertyPlaceholderHelper.PlaceholderResolver() {
            @Override
            public String resolvePlaceholder(String placeholderName) {
                return getAttr(placeholderName);
            }
        });
    }

    @Override
    public String[] getArrayAttr(String key) {
        return getAttr(key, String[].class);
    }

    @Override
    public <T> T getAttr(String key, Class<T> targetType, T defaultValue) {
        T value = getAttr(key, targetType);
        return value == null ? defaultValue : value;
    }

    @Override
    public <T> T getRequiredAttr(String key, Class<T> targetType) throws IllegalStateException {
        T value = getAttr(key, targetType);
        if (value == null) {
            throw new IllegalStateException("required key [" + key + "] not found");
        }
        return value;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<String, String> getAttrs(String... keys) {
        return getAttrs(keys == null ? Collections.EMPTY_LIST : Arrays.asList(keys));
    }

    @Override
    public Map<String, String> getAttrs(Collection<String> keys) {
        if (CollectionUtils.isEmpty(keys)) {
            keys = Arrays.asList(getKeys());
        }
        Map<String, String> map = Maps.newHashMapWithExpectedSize(keys.size());
        for (String key : keys) {
            String value = getAttr(key);
            if (value != null) {
                map.put(key, value);
            }
        }
        return Collections.unmodifiableMap(map);
    }

    @Override
    public void setAttrs(Map<String, ?> map) {
        for (Map.Entry<String, ?> entry : map.entrySet()) {
            setAttr(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public String toString() {
        return getAttrs().toString();
    }
}
