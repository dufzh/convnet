package net.convnet.server.support.attr;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.util.TypeUtils;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-11-6
 */
public final class FastjsonConverter implements Converter {
    public static Converter INSTANCE = new FastjsonConverter();

    @Override
    @SuppressWarnings("unchecked")
    public <T> T convert(Object value, Class<T> targetType) {
        if (value == null) {
            return null;
        }
        Class valueClass = value.getClass();
        if (targetType == valueClass) {
            return (T) value;
        }
        if (targetType == Object.class) {
            return (T) value;
        }
        if (targetType.isAssignableFrom(valueClass)) {
            return (T) value;
        }
        if (targetType == String.class) {
            return valueClass == String.class ? (T) value : (T) JSON.toJSONString(value);
        }
        if (value instanceof String) {
            return JSON.parseObject((String) value, targetType);
        } else {
            return TypeUtils.castToJavaBean(JSON.toJSON(value), targetType);
        }
    }
}
