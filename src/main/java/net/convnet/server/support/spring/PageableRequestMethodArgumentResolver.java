package net.convnet.server.support.spring;

import org.springframework.core.MethodParameter;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableArgumentResolver;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-3-28
 */
public class PageableRequestMethodArgumentResolver implements HandlerMethodArgumentResolver {

    private PageableArgumentResolver pageableArgumentResolver = new PageableArgumentResolver();

    public void setPageableArgumentResolver(PageableArgumentResolver pageableArgumentResolver) {
        this.pageableArgumentResolver = pageableArgumentResolver;
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().equals(Pageable.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        return pageableArgumentResolver.resolveArgument(parameter, webRequest);
    }
}
