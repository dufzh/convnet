package net.convnet.server.session;

import io.netty.channel.Channel;
import net.convnet.server.identity.User;
import net.convnet.server.support.attr.Attrable;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
public interface Session extends Attrable {

    long getId();

    boolean isLogin();

    int getUserId();

    User getUser();

    String getIp();

    int getPort();

    String getMac();

    Channel getChannel();

    int getProtocolVersion();

    void destory();

    long getReadBytes();

    long getWriteBytes();
}
