package net.convnet.server.session.impl;

import io.netty.channel.Channel;
import io.netty.util.AttributeKey;
import net.convnet.server.Constants;
import net.convnet.server.session.Session;
import net.convnet.server.support.attr.JSONObjectAttrable;

import java.net.InetSocketAddress;
import java.util.concurrent.atomic.AtomicLong;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
abstract class DefaultSession extends JSONObjectAttrable implements Session {
    private static final long serialVersionUID = 6278446630616735565L;
    static AttributeKey<Session> ATTR_KEY = new AttributeKey<Session>("CONVNET_SESSION");
    private static final AtomicLong COUNTER = new AtomicLong();
    private final long id;
    private final int userId;
    private final Channel channel;

    private String ip;
    private Integer port;
    private String mac;
    private int protocolVersion;
    private boolean closed;


    public DefaultSession(int userId, Channel channel) {
        this.userId = userId;
        this.channel = channel;
        this.id = COUNTER.incrementAndGet();
        this.channel.attr(ATTR_KEY).set(this);
    }

    public long getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public Channel getChannel() {
        return channel;
    }

    public String getIp() {
        if (ip == null) {
            fillAddress();
        }
        return ip;
    }

    public int getPort() {
        if (port == null) {
            fillAddress();
        }
        return port;
    }

    private void fillAddress() {
        InetSocketAddress address = (InetSocketAddress) channel.remoteAddress();
        ip = address.getAddress().getHostAddress();
        port = address.getPort();
    }

    public String getMac() {
        return getAttr("mac");
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public int getProtocolVersion() {
        return protocolVersion;
    }

    public void setProtocolVersion(int protocolVersion) {
        this.protocolVersion = protocolVersion;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    @Override
    public void destory() {
        channel.attr(ATTR_KEY).remove();
    }

    @Override
    public long getWriteBytes() {
        return def(channel.attr(Constants.WRITE_BYTES_KEY).get(), 0);
    }

    @Override
    public long getReadBytes() {
        return def(channel.attr(Constants.READ_BYTES_KEY).get(), 0);
    }

    private long def(Long l, long def) {
        return l == null ?  def:l;
    }
}
