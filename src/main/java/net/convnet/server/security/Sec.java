package net.convnet.server.security;

import net.convnet.server.Constants;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-9-26
 */
public final class Sec {

    private Sec() {
    }

    public static Integer getUserId() {
        return SecContext.getContext().get(Constants.USER_ID);
    }

}
