package net.convnet.server.security;

import java.util.HashMap;
import java.util.Map;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-9-24
 */
public final class SecContext {
    private static ThreadLocal<SecContext> LOCAL = new InheritableThreadLocal<SecContext>() {
        @Override
        protected SecContext initialValue() {
            return new SecContext();
        }
    };

    private final Map<String, Object> values = new HashMap<String, Object>();

    private SecContext() {
    }

    public static SecContext getContext() {
        return LOCAL.get();
    }

    public static void clearContext() {
        LOCAL.remove();
    }

    public SecContext set(String key, Object value) {
        if (value == null) {
            values.remove(key);
        } else {
            values.put(key, value);
        }
        return this;
    }

    public SecContext remove(String key) {
        values.remove(key);
        return this;
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key) {
        return (T) values.get(key);
    }
}
