package net.convnet.server.security;


import net.convnet.server.Constants;
import net.convnet.server.support.spring.ConfigurableInterceptor;
import net.convnet.server.util.RequestUtils;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-9-26
 */
public class SecContextInterceptor extends ConfigurableInterceptor {

    private String[] needLogins;
    private String redirectUrl;

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public void setNeedLogins(String[] needLogins) {
        this.needLogins = needLogins;
    }

    @Override
    public boolean internalPreHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        SecContext.getContext().set(Constants.USER_ID, request.getSession().getAttribute(Constants.USER_ID));
        if (RequestUtils.matchAny(request, urlPathHelper, pathMatcher, needLogins) && Sec.getUserId() == null) {
            if (redirectUrl != null) {
                if (redirectUrl.startsWith("/") && !redirectUrl.startsWith(request.getContextPath())) {
                    redirectUrl = request.getContextPath() + redirectUrl;
                }
                response.sendRedirect(redirectUrl + (redirectUrl.contains("?") ? "&" : "?") + "url=" + ServletUriComponentsBuilder.fromRequest(request).build().encode());
            } else {
                throw new RuntimeException("Need login");
            }
            return false;
        }
        return true;
    }

    @Override
    public void internalAfterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        SecContext.clearContext();
    }
}
