package net.convnet.server.processor;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.identity.FindType;
import net.convnet.server.identity.Group;
import net.convnet.server.identity.User;
import net.convnet.server.protocol.AbstractProcessor;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.Request;
import net.convnet.server.protocol.Response;
import net.convnet.server.session.Session;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
@Service
public class CreateGroupProcessor extends AbstractProcessor {
    @Override
    public Cmd accept() {
        return Cmd.CREATE_GROUP;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {
        Group group= groupManager.getGroupByName(request.getParam("groupname"));

        if (group!=null){
            response.setAttr("iscreated","F");
        }
        else
        {


            Group group1= new Group();
            group1.setName(request.getParam("groupname"));
            User user= session.getUser();

            if (user.isCanCreateGroup())
            {
                group1.setCreator(user);
                group1.setAdmins(Collections.singleton(user));
                group1.setPassword(request.getParam("gourppass"));
                group1.setDescription(request.getParam("groupdesc"));
                group1.setUsers(Collections.singletonList(user));
                groupManager.saveGroup(group1);
                user.getGroups().add(group1);
                userManager.saveUser(user);
                response.setAttr("iscreated","S");
            }
            else
            {
                response.setAttr("iscreated","N");
            }

        }
       // write(session,response);
    }
}
