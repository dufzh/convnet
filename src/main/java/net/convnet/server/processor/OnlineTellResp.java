package net.convnet.server.processor;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.identity.Group;
import net.convnet.server.identity.User;
import net.convnet.server.protocol.*;
import net.convnet.server.session.Session;
import org.springframework.stereotype.Service;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
@Service
public class OnlineTellResp extends AbstractProcessor {

    @Override
    public Cmd accept() {
        return Cmd.ONLINE_TELL;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {
        TellUserOnline(session.getUser());
    }

    private void TellUserOnline(User user)
    {
        int  userid=user.getId();
        for (User tmpuser : user.getFriends()){
            Session session = sessionManager.getSession(tmpuser.getId());
            if (session!=null){
                Response resposne1 = createResponse(session,Cmd.ONLINE_TELL_RESP);
                resposne1.setAttr("who",userid);
                write(session,resposne1);
            }
        }
        for (Group group1: user.getGroups()){
            for (User user1:group1.getUsers())
            {
                int userid2= user1.getId();
                if(userid!=userid2)//忽略本人
                {
                    Session session = sessionManager.getSession(user1.getId());
                    if (session!=null){
                        Response resposne1 = createResponse(session,Cmd.ONLINE_TELL_RESP);
                        resposne1.setAttr("who",userid);
                        write(session,resposne1);
                    }
                }
            }
        }
    }


}
