package net.convnet.server.processor;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.identity.FindType;
import net.convnet.server.protocol.AbstractProcessor;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.identity.User;
import net.convnet.server.protocol.Request;
import net.convnet.server.protocol.Response;
import net.convnet.server.session.Session;
import org.springframework.stereotype.Service;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
@Service
public class FindUserProcessor extends AbstractProcessor {
    @Override
    public Cmd accept() {
        return Cmd.FIND_USER;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {
        User user=session.getUser();
        if (user.isCanAddfriend()){
            response.setAttr("users", userManager.findUser(FindType.from(request.getParam("type")), request.getParam("value"), 20));
        }
        else
        {
            response.setAttr("users",null);
        }
    }
}
