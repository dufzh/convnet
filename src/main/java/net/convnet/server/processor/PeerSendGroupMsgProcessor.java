package net.convnet.server.processor;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.identity.Group;
import net.convnet.server.identity.User;
import net.convnet.server.protocol.AbstractProcessor;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.Request;
import net.convnet.server.protocol.Response;
import net.convnet.server.session.Session;
import org.springframework.stereotype.Service;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
@Service
public class PeerSendGroupMsgProcessor extends AbstractProcessor {

    @Override
    public Cmd accept() {
        return Cmd.SEND_GROUP_MSG;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {
        int targetgroupid = request.getIntParam("groupid");
        Group targetgroup = groupManager.getGroup(targetgroupid);

        for (User targetuser:targetgroup.getUsers()) {
            int targetuserid = targetuser.getId();
            Session targetsession = sessionManager.getSession(targetuserid);
            response.setOutput(false);
            if (targetsession == null) {
               // do nothing
            } else {
                Response response1 = createResponse(targetsession, Cmd.SEND_GROUP_MSG_RESP);
                response1.setAttr("groupid", targetgroupid);
                response1.setAttr("userid", session.getUserId());
                response1.setAttr("msg", request.getParam("msg"));
                write(targetsession, response1);
            }
        }

    }


}
