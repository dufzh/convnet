package net.convnet.server.processor;

import net.convnet.server.email.Email;
import net.convnet.server.email.EmailSender;
import net.convnet.server.ex.ConvnetException;
import net.convnet.server.ex.EntityExistsException;
import net.convnet.server.identity.ResetCode;
import net.convnet.server.identity.ResetCodeManager;
import net.convnet.server.identity.User;
import net.convnet.server.identity.UserManager;
import net.convnet.server.protocol.*;
import net.convnet.server.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-13
 */
@Service
public class RegisterProcessor extends AbstractProcessor {
    @Override
    public Cmd accept() {
        return Cmd.REGIST_USER;
    }

    @Value("#{props.allowregist}")
    private boolean allowregist;

    @Value("#{props.canCreateGroup}")
    private boolean canCreateGroup;

    @Value("#{props.canJoinGroup}")
    private boolean canJoinGroup;

    @Autowired
    private ResetCodeManager resetCodeManager;

    @Value("#{props['reset.url']}")
    private String resetUrl;

    @Value("#{props['forceUseMailCheck']}")
    private boolean forceUseMailCheck;

    @Value("#{props['maxRigistCount']}")
    private int maxRigistCount;

    @Autowired
    private EmailSender emailSender;

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {
        if (allowregist)
        {
            String name = request.getRequiredParam("name");
            User user = userManager.getUserByName(request.getRequiredParam("name"));

            if (user != null) {
                response.setAttr("status","F");
                return;
                //throw new EntityExistsException(User.class, "name=" + name);
            }

            //session.getIp()
            if (maxRigistCount>0) {
                int count = userManager.getTodayRegistUserCountFromIp(session.getIp());
                if (count >= maxRigistCount) {
                    response.setAttr("status", "F");
                    response.setAttr("info", "N");
                    return;
                }
            }

            String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";

            if (forceUseMailCheck) {
                if (!name.matches(EMAIL_REGEX)) {
                    response.setAttr("status","F");
                    response.setAttr("info","M");
                    return;
                }
            }

            user = new User();
            user.setName(name);user.setName(name);
            user.setNickName(request.getParam("nickName"));
            if (forceUseMailCheck) {
                user.setPassword("waitForMailConfirm");
            }else
            {
                user.setPassword(request.getRequiredParam("password"));
            }

            user.setDescription(request.getParam("description"));
            user.setRegisterIp(session.getIp());
            user.setCanAddfriend(canJoinGroup);
            user.setCanCreateGroup(canCreateGroup);

            userManager.saveUser(user);


            if (user!=null && forceUseMailCheck) {
                ResetCode resetCode = resetCodeManager.createResetCode(user);
                Email e = new Email();
                e.setTo(new String[]{name});
                e.setSubject("ConVNet 密码设置");
                String url = resetUrl + resetCode.getId();
                e.setBody("用户: " + user.getName() + ",请点击 <a href=" + url + " target=\"_blank\">" + url + "</a> 设置Convnet密码");
                emailSender.send(e);
            }

            response.setAttr("status","T");
        }else
        {
            response.setAttr("status","F");
            response.setAttr("info","N");
        }
    }
}
