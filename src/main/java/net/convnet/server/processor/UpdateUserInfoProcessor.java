package net.convnet.server.processor;

import io.netty.util.internal.StringUtil;
import net.convnet.server.ex.ConvnetException;
import net.convnet.server.identity.User;
import net.convnet.server.protocol.AbstractProcessor;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.Request;
import net.convnet.server.protocol.Response;
import net.convnet.server.session.Session;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-14
 */
@Service
public class UpdateUserInfoProcessor extends AbstractProcessor {

    @Override
    public Cmd accept() {
        return Cmd.RENEW_MY_INFO;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {
        //nickName,password,description,p1,p2,p3,p4
        User user = session.getUser();
        user.setNickName(request.getParam("nickName"));
        user.setAllowpass1(request.getParam("p1"));
        user.setAllowpass2(request.getParam("p2"));
        user.setFriendpass(request.getParam("p3"));
        user.setDospass(request.getParam("p4"));
        user.setDescription(request.getParam("description"));
        //user.setConnectPasswords();
        userManager.saveUser(user);
        String tmpstr= StringUtils.trim(request.getParam("password"));
        //userManager.setPassword(user,request.getParam("password"));
        if (!(tmpstr.equals("　")||tmpstr.equals("")||tmpstr.equals("NOTMODIFYED")))
        {
            userManager.updatePassword(user.getId(),request.getParam("password"));
        }
    }
}
