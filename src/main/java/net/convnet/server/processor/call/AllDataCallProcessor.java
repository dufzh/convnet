package net.convnet.server.processor.call;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.protocol.CallProcessor;
import net.convnet.server.protocol.P2PCallType;
import net.convnet.server.protocol.Request;
import net.convnet.server.protocol.Response;
import net.convnet.server.session.Session;
import org.springframework.stereotype.Service;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-27
 */
@Service
public class AllDataCallProcessor implements CallProcessor{
    @Override
    public P2PCallType accept() {
        return P2PCallType.ALL_DATA;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {
        //todo:impl it

    }
}
