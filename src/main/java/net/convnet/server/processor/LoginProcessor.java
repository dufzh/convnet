package net.convnet.server.processor;

import io.netty.handler.traffic.ChannelTrafficShapingHandler;
import net.convnet.server.ex.ConvnetException;
import net.convnet.server.identity.FriendRequest;
import net.convnet.server.identity.Group;
import net.convnet.server.identity.User;
import net.convnet.server.identity.UserEx;
import net.convnet.server.protocol.AbstractProcessor;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.Request;
import net.convnet.server.protocol.Response;
import net.convnet.server.session.Session;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
@Service
public class LoginProcessor extends AbstractProcessor {

    @Override
    public Cmd accept() {
        return Cmd.LOGIN;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {
        if (session.isLogin()) {
            throw new ConvnetException("Already logined");
        }


        User user = null;
        try {
           user = userManager.validateUser(request.getParam("name"), request.getParam("password"));
        } catch (ConvnetException e) {
           if (e.getMessage()=="USER_DISABLED")
           {
               response.setAttr("status","R");
               return;
           }

            if (e.getMessage()=="USER_ERRORPASS")
            {
                response.setAttr("status","F");
                return;
            }
            if (e.getMessage()=="USER_NOUSER")
            {
                response.setAttr("status","F");
                return;
            }
           // e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        if (user==null){
            response.setAttr("status", "F");
            return;
        }

        session = sessionManager.createSession(user.getId(), session.getChannel());

        session.setAttr("mac", request.getParam("mac"));
        response.setAttr("status","T");
        response.setAttr("ip", session.getIp());
        response.setAttr("id", user.getId());
        response.setAttr("nickName", user.getNickName());
        response.setAttr("name", user.getName());
        response.setAttr("password", "");
        response.setAttr("description", user.getDescription());
        String[] cp = user.getConnectPasswords();
        int len = cp == null ? 0 : cp.length;
        if (len > 0) response.setAttr("p1", cp[0]);
        if (len > 1) response.setAttr("p2", cp[1]);
        if (len > 2) response.setAttr("p3", cp[2]);
        if (len > 3) response.setAttr("p4", cp[3]);

        if ((user.getReciveLimit()!=0) || (user.getSendLimit()!=0))
        {
            session.getChannel().pipeline().remove("speedLimit");
            session.getChannel().pipeline().addAfter("handshake","speedLimit",new ChannelTrafficShapingHandler(user.getReciveLimit(),0));
        }

        UserEx userEx = user.getUserEx();
        if (userEx == null) {
            userEx = new UserEx();
            userEx.setUser(user);
        }
        userEx.setLastLoginAt(new Date());
        userEx.setLastLoginIp(session.getIp());
        userEx.setUserIsOnline(true);


        userManager.saveUserEx(userEx);

        //好友申请
        for (FriendRequest friendRequest:user.getFriendRequests())
        {
            String description= friendRequest.getDescription();
            Response notify = createResponse(session, Cmd.PEER_ORD_FRIEND_RESP);
            User user1=friendRequest.getUser();
            notify.setAttr("count", "1")
                    .setAttr("userId", user1.getId())
                    .setAttr("nickName", user1.getName())
                    .setAttr("description", description);
            write(session, notify);
        }

    }

}
