package net.convnet.server.processor;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.identity.Group;
import net.convnet.server.identity.User;
import net.convnet.server.protocol.AbstractProcessor;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.Request;
import net.convnet.server.protocol.Response;
import net.convnet.server.session.Session;
import org.springframework.stereotype.Service;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
@Service
public class QuitGroupProcessor extends AbstractProcessor {
    @Override
    public Cmd accept() {
        return Cmd.QUIT_GROUP;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {
        int targetGroup = request.getIntParam("groupid");


        Group group = groupManager.getGroup(targetGroup);


        if (group==null){return;}

        //如果是管理员则解散本组
        if (session.getUser()==group.getCreator())
        {
            response.setOutput(false);
            for (User user:group.getUsers())
            {
                Session session1=sessionManager.getSession(user.getId());
                if (session1==null){continue;}

                Response response1=createResponse(session1,Cmd.QUIT_GROUP_RESP);
                response1.setAttr("msgtype","groupdismiss");
                response1.setAttr("groupid",targetGroup);
                write(session1,response1);
            }
            groupManager.removeGroup(group.getId());
        }
        else
        {

            //通知用户用户退出本组
            for (User user:group.getUsers())
            {
                Session session1=sessionManager.getSession(user.getId());
                if (session1==null){continue;}

                Response response1=createResponse(session1,Cmd.QUIT_GROUP_RESP);
                response1.setAttr("msgtype","userquit");
                response1.setAttr("groupid", targetGroup);
                response1.setAttr("userid", session.getUserId());
                write(session1,response1);
            }
            //退出用户组
            groupManager.quitGroup(session.getUser(),group);


        }

    }
}
