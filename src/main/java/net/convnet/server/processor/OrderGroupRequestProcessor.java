package net.convnet.server.processor;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.identity.Group;
import net.convnet.server.identity.User;
import net.convnet.server.protocol.AbstractProcessor;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.Request;
import net.convnet.server.protocol.Response;
import net.convnet.server.session.Session;
import org.springframework.stereotype.Service;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
@Service
public class OrderGroupRequestProcessor extends AbstractProcessor {
    @Override
    public Cmd accept() {
        return Cmd.JOIN_GROUP;
    }

    @Override
    public void process(Session session, Request request, Response response) throws ConvnetException {
        int targetGroup = request.getIntParam("groupid");
        String description = request.getParam("description");

        response.setOutput(false);
        Group group = groupManager.getGroup(targetGroup);
        User user = session.getUser();
        User admin = group.getCreator();
        String grouppass=group.getPassword();

        //如果入群密码匹配则允许直接加入
        if (grouppass.equals(description) && (! description.equals("")))
        {
           //group.getUsers().add(user);
           //groupManager.saveGroup(group);
            groupManager.joinGroup(user,group);
            for (User groupuser:group.getUsers())
            {
                response.setOutput(false);
                //通知组内成员有新成员加入
                int userid=groupuser.getId();
                if (sessionManager.isOnline(userid))
                {
                    Session targetSession = sessionManager.getSession(userid);
                    Response notify = createResponse(targetSession, Cmd.PEER_SURE_JOIN_GROUP_RESP);
                    notify.setAttr("groupid",group.getId());
                    notify.setAttr("whoid",user.getId());
                    notify.setAttr("name",user.getName());
                    notify.setAttr("isonline",sessionManager.isOnline(user.getId())?"T":"F");
                    System.out.print("userjoinnotify:"+userid);
                    write(targetSession, notify);
                }
            }


            return;
        }

         groupManager.sendGroupRequest(user.getId(),group.getId(),description);
        if (sessionManager.isOnline(admin.getId()))
        {
            //username,userid,gourpid,gourpdesc,orderdesc
            Response resposne2 = createResponse(session,Cmd.JOIN_GROUP_RESP);
            resposne2.setAttr("username",user.getName());
            resposne2.setAttr("userid",user.getId());
            resposne2.setAttr("gourpid",targetGroup);
            resposne2.setAttr("gourpdesc",group.getDescription());
            resposne2.setAttr("orderdesc",description);

            //将申请信息发送给管理员
            write(sessionManager.getSession(admin.getId()),resposne2);
        }



    }
}
