package net.convnet.server.identity;

import java.util.List;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-12
 */
public interface GroupManager {

    Group getGroup(int id);

    Group getGroupByName(String groupName);

    List<Group> findGroup(FindType type, String value, int size);

    Group saveGroup(Group group);

    void removeGroup(int id);

    void sendGroupRequest(int userId, int targetGroupId, String description);

    GroupRequest dealGroupRequest(int userid,int groupid, boolean reject);

    GroupRequest getGroupRequest(int userId,int groupid);

    void joinGroup(User user,Group group);

    void quitGroup(User user,Group group);

    List<Group> getAllGroup();
}
