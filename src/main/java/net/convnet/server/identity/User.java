package net.convnet.server.identity;

import io.netty.channel.Channel;
import net.convnet.server.protocol.Protocol;
import net.convnet.server.session.Session;
import net.convnet.server.session.SessionManager;
import net.convnet.server.support.hibernate.JSONType;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-7
 */
@Entity
@Table(name = "cvn_user")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class User implements Serializable {
    private static final long serialVersionUID = 4540506311726693246L;
    public static final int ANONYMOUS_ID = 0;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "user_gen")
    @TableGenerator(
            name = "user_gen",
            allocationSize = 1
    )
    private Integer id;
    @Column(length = 64)
    @NaturalId
    private String name;
    @Column(length = 64)
    private String nickName;
    @Column(length = 256)
    private String description;
    @Column(length = 64)
    private String email;
    @Column(length = 256)
    private String password;
    @Column(length = 64)
    private String passwordHash;

    @Column(length = 64)
    private String allowpass1;
    @Column(length = 64)
    private String allowpass2;
    @Column(length = 64)
    private String friendpass;
    @Column(length = 64)
    private String dospass;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date createAt;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date updateAt;
    @Column(length = 32)
    private String registerIp;
    private boolean admin;
    private boolean enabled = true;

    private long reciveLimit;
    private long sendLimit;

    public long getReciveLimit() {
        return reciveLimit;
    }

    public void setReciveLimit(long reciveLimit) {
        this.reciveLimit = reciveLimit;
    }

    public long getSendLimit() {
        return sendLimit;
    }

    public void setSendLimit(long sendLimit) {
        this.sendLimit = sendLimit;
    }

    public boolean isCanCreateGroup() {
        return canCreateGroup;
    }

    public void setCanCreateGroup(boolean canCreateGroup) {
        this.canCreateGroup = canCreateGroup;
    }

    public boolean isCanAddfriend() {
        return canAddfriend;
    }

    public void setCanAddfriend(boolean canAddfriend) {
        this.canAddfriend = canAddfriend;
    }

    private boolean canCreateGroup;
    private boolean canAddfriend;


    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private UserEx userEx;
    @ManyToMany(mappedBy = "users")
    @OrderBy("name")
    // @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<Group> groups;
    @ManyToMany
    @JoinTable(name = "cvn_friend", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = {@JoinColumn(name = "friend_id")})
    @OrderBy("name")
    // @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<User> friends;
    @OneToMany(mappedBy = "target")
    private List<FriendRequest> friendRequests;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }




    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String[] getConnectPasswords() {
        String[] passwords= new String[4];
        passwords[0]=allowpass1;
        passwords[1]=allowpass2;
        passwords[2]=friendpass;
        passwords[3]=dospass;
        return passwords;
    }

    public void setAllowpass1(String str){
        allowpass1=str;
    }

    public void setAllowpass2(String str){
        allowpass2=str;
    }

    public void setFriendpass(String str){
        friendpass=str;
    }


    public void setDospass(String str){
        dospass=str;
    }


    public void setConnectPasswords(String[] connectPasswords) {
        allowpass1=connectPasswords[0];
        allowpass2=connectPasswords[1];
        friendpass=connectPasswords[2];
        dospass=connectPasswords[3];
        //this.connectPasswords = connectPasswords;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getRegisterIp() {
        return registerIp;
    }

    public void setRegisterIp(String registerIp) {
        this.registerIp = registerIp;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public UserEx getUserEx() {
        return userEx;
    }

    public void setUserEx(UserEx userEx) {
        this.userEx = userEx;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public List<User> getFriends() {
        return friends;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }

    public List<FriendRequest> getFriendRequests() {
        return friendRequests;
    }

    public void setFriendRequests(List<FriendRequest> friendRequests) {
        this.friendRequests = friendRequests;
    }

    public String getAllowpass1(){
        return allowpass1;
    }

    public String getAllowpass2(){
        return allowpass2;
    }

    public String getFriendpass(){
        return friendpass;
    }

    public String getDospass(){
        return dospass;
    }



    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof User && name.equals(((User) o).getName());
    }



}
