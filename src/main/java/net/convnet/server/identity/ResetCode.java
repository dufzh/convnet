package net.convnet.server.identity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oznyang@163.com">oznyang</a>
 * @version V1.0, 14-1-14
 */
@Entity
@Table(name = "cvn_reset_code")
public class ResetCode implements Serializable {
    private static final long serialVersionUID = 5721789499123284672L;
    @Id
    @Column(length = 32)
    private String id;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date createAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date expireAt;

    @OneToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getExpireAt() {
        return expireAt;
    }

    public void setExpireAt(Date expireAt) {
        this.expireAt = expireAt;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
