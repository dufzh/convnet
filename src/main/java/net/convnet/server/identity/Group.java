package net.convnet.server.identity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-7
 */
@Entity
@Table(name = "cvn_group")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Group implements Serializable {
    private static final long serialVersionUID = 3035250159415432203L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(length = 64, nullable = false, unique = true)
    @NaturalId
    private String name;
    @Column(length = 256)
    private String description;
    @Column(length = 64)
    private String password;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date createAt;
    private boolean enabled = true;

    @ManyToOne
    @JoinColumn(name = "creator_id", nullable = false)
    private User creator;
    @ManyToMany
    @JoinTable(name = "cvn_group_user", joinColumns = @JoinColumn(name = "group_id"), inverseJoinColumns = {@JoinColumn(name = "user_id")})
    @OrderBy("name")
    // @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<User> users;
    @ManyToMany
    @JoinTable(name = "cvn_group_admin", joinColumns = @JoinColumn(name = "group_id"), inverseJoinColumns = {@JoinColumn(name = "user_id")})
    private Set<User> admins;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Set<User> getAdmins() {
        return admins;
    }

    public void setAdmins(Set<User> admins) {
        this.admins = admins;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Group && name.equals(((Group) o).getName());
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
