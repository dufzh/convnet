package net.convnet.server.identity.impl;

import net.convnet.server.identity.ResetCode;
import net.convnet.server.identity.ResetCodeManager;
import net.convnet.server.identity.User;
import net.convnet.server.support.hibernate.HibernateRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oznyang@163.com">oznyang</a>
 * @version V1.0, 14-1-14
 */
public class ResetCodeManagerImpl extends HibernateRepository<ResetCode, String> implements ResetCodeManager {

    @Override
    @Transactional
    public ResetCode findByUserID(int userid){
        List<ResetCode> list=list(criteria(Restrictions.eq("user.id", userid)).setMaxResults(1));
        if (list.size()==0){ return null;}
        return list.get(0);
    }

    @Override
    @Transactional
    public ResetCode createResetCode(User user) {
        ResetCode resetCode;

        resetCode = findByUserID(user.getId());

        //取用户没有使用的记录，修改过期时间
        if (resetCode==null) {
            resetCode = new ResetCode();
            resetCode.setId(RandomStringUtils.randomAlphanumeric(8));
            resetCode.setUser(user);
            //当前时间+1H
            resetCode.setCreateAt(new Date());
            resetCode.setExpireAt(DateUtils.addHours(resetCode.getCreateAt(), 1));
            save(resetCode);
        }
        else {
            //当前时间+1H
            resetCode.setCreateAt(new Date());
            resetCode.setExpireAt(DateUtils.addHours(resetCode.getCreateAt(), 1));
            save(resetCode);
        }
        return resetCode;
    }


    @Override
    @Transactional(readOnly = true)
    public User getUserByResetCode(String code) {
        ResetCode resetCode = get(code);
        if (resetCode == null || resetCode.getExpireAt().getTime() < System.currentTimeMillis()) {
            return null;
        }
        return resetCode.getUser();
    }

    @Override
    @Transactional
    public void removeResetCode(String code) {
        deleteByPK(code);
    }
}
