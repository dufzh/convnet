package net.convnet.server.identity.impl;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.ex.EntityNotFoundException;
import net.convnet.server.identity.*;
import net.convnet.server.support.encrypt.EncryptService;
import net.convnet.server.support.hibernate.HibernateRepository;
import net.convnet.server.util.Codecs;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-12
 */
@Transactional(readOnly = true)
public class UserManagerImpl extends HibernateRepository<User, Integer> implements UserManager {
    public String salt = "_t[er,z59)]x7c&d;x24v%";
    private EncryptService encryptService;

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public void setEncryptService(EncryptService encryptService) {
        this.encryptService = encryptService;
    }

    @Override
    public User getUser(int id) {
        return get(id);
    }

    @Override
    public User getUserByName(String userName) {

        Criteria criteria = criteria();

        List list=list(criteria.add(Restrictions.eq("name", userName).ignoreCase()));
        if (list.size()==0){
            return null;
        }else {
            return (User) list.get(0);
        }

    }

    @Override
    public List<User> findUser(FindType type, String value, int size) {
        String fieldName = null;
        switch (type) {
            case NAME:
                fieldName = "name";
                break;
            case NICK_NAME:
                fieldName = "nickName";
                break;
            case DESCRIPTION:
                fieldName = "description";
                break;
        }
        return list(criteria(Restrictions.like(fieldName, value, MatchMode.ANYWHERE)).addOrder(Order.asc("name")).setMaxResults(size));
    }

    @Override
    public Page<User> findUser(String name, Pageable request,boolean isOnline) {
        Criteria criteria = criteria();
        if (StringUtils.isNotEmpty(name)) {
            criteria.add(Restrictions.like("name", name, MatchMode.ANYWHERE));
        }

        if (isOnline){
            criteria.createAlias("userEx", "userEx");
            criteria.add(Restrictions.eq("userEx.userIsOnline",isOnline));
        }

        return find(criteria.addOrder(Order.desc("id")), request);
    }

    @Override
    public User validateUser(String userName, String password) throws ConvnetException {
        password = formatPassword(password);
        User user = getUserByName(userName);
        if (user == null  ) {
            throw new ConvnetException("USER_NOUSER");
        }
        if (!user.isEnabled())
        {
            throw new ConvnetException("USER_DISABLED");
        }
        if (!user.getPasswordHash().equals(Codecs.hashHex(password + salt))) {
            throw new ConvnetException("USER_ERRORPASS");
        }
        return user;
    }

    @Override
    @Transactional
    public void updatePassword(int id, String password) {
        User user = getUser(id);
        setPassword(user, password);
        saveUser(user);
    }

    private static String formatPassword(String password) {
        password = StringUtils.trimToNull(password);
        if (password == null) {
            throw new IllegalArgumentException("Password can not be null");
        }
        return password;
    }

    public void setPassword(User user, String password) {
        password = formatPassword(password);
        user.setPasswordHash(Codecs.hashHex(password + salt));
        try {
            user.setPassword(encryptService.encrypt(password));
        } catch (GeneralSecurityException ignored) {
        }
    }

    @Override
    @Transactional
    public User saveUser(User user) {
        if (user.getId() == null) {
            user.setCreateAt(new Date());
            UserEx userEx = new UserEx();
            userEx.setUser(user);
            user.setUserEx(userEx);
            setPassword(user, user.getPassword());
        }
        user.setUpdateAt(new Date());
        return save(user);
    }

    @Override
    @Transactional
    public UserEx saveUserEx(UserEx userEx) {
        getSession().saveOrUpdate(userEx);
        return userEx;
    }

    @Override
    public int getTodayRegistUserCountFromIp(String IP) {

        String sql="select count(*) from cvn_user a where a.Create_at>? and a.register_ip='"+IP+"'";

        Query query = getSession().createSQLQuery(sql);
        query.setDate(0,DateUtils.truncate(new Date(), Calendar.DATE));


        BigInteger count = (BigInteger)query.uniqueResult();


       return  count.intValue();
    }

    @Override
    @Transactional
    public void removeUser(int id) {
        deleteByPK(id);
    }

    @Override
    @Transactional
    public void sendFriendRequest(int userId, int targetUserId, String description) {
        FriendRequest request = getFriendRequest(userId, targetUserId);
        if (request == null) {
            request = new FriendRequest();
            request.setCreateAt(new Date());
            request.setTarget(getUser(targetUserId));
            request.setUser(getUser(userId));
        }
        request.setDescription(description);
        getSession().persist(request);
    }

    @Override
    @Transactional
    public void dealFriendRequest(int userId, int targetUserId, boolean reject) {
        //获取申请信息，如果没有则属于假冒信息
        FriendRequest request = getFriendRequest(userId, targetUserId);
        if (request == null) {
            throw new EntityNotFoundException(FriendRequest.class, "userId=" + userId + ",targetUserId=" + targetUserId);
        }

        if (!reject) {
            User user = request.getUser();
            User target = request.getTarget();
            user.getFriends().add(target);
            target.getFriends().add(user);
            saveUser(user);
            saveUser(target);
        }
        getSession().delete(request);
    }

    private FriendRequest getFriendRequest(int userId, int targetUserId) {
        Criteria criteria = getSession().createCriteria(FriendRequest.class);
        criteria.add(Restrictions.eq("user.id", userId));
        criteria.add(Restrictions.eq("target.id", targetUserId));
        return (FriendRequest) criteria.uniqueResult();
    }
}
