package net.convnet.server.protocol;

import java.util.Map;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-14
 */
public interface Response extends ResponseReader {

    void setCmd(Cmd cmd);

    void setSuccess(boolean success);

    Response setAttr(String name, Object value);

    Response setAttrs(Map<String, Object> attrs);
}
