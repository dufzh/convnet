package net.convnet.server.protocol;

import com.alibaba.fastjson.JSONObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-14
 */
public final class DefaultRequest implements Request, RequestBuilder, Serializable {
    private static final long serialVersionUID = 3348082539153902075L;
    private final Map<String, Object> attrs = new HashMap<String, Object>();
    private final JSONObject params = new JSONObject();

    private final int version;
    private final Cmd cmd;

    public DefaultRequest(Cmd cmd,int version) {
        this.cmd = cmd;
        this.version = version;
    }

    public int getVersion() {
        return version;
    }


    public Cmd getCmd() {
        return cmd;
    }

    @Override
    public RequestBuilder set(String name, Object value) {
        params.put(name, value);
        return this;
    }

    @Override
    public RequestBuilder set(Map<String, Object> map) {
        params.putAll(map);
        return this;
    }

    @Override
    public boolean hasAttr(String name) {
        return attrs.containsKey(name);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getAttr(String name) {
        return (T) attrs.get(name);
    }

    @Override
    public Request setAttr(String name, Object value) {
        attrs.put(name, value);
        return this;
    }

    @Override
    public boolean hasParam(String name) {
        return params.containsKey(name);
    }

    @Override
    public String getParam(String name) {
        return params.getString(name);
    }

    @Override
    public Object getRawParam(String name) {
        return params.get(name);
    }

    @Override
    public String getParam(String name, String defaultValue) {
        return def(getParam(name), defaultValue);
    }

    @Override
    public String getRequiredParam(String name) throws IllegalStateException {
        return required(name, getParam(name));
    }

    @Override
    public String[] getParams(String name) {
        return getParam(name, String[].class);
    }

    @Override
    public Integer getIntParam(String name) {
        return params.getInteger(name);
    }

    @Override
    public int getRequiredIntParam(String name) throws IllegalStateException {
        return required(name, getIntParam(name));
    }

    @Override
    public int getIntParam(String name, int defaultValue) {
        return def(getIntParam(name), defaultValue);
    }

    @Override
    public <T> T getParam(String name, Class<T> targetType) {
        return params.getObject(name, targetType);
    }

    @Override
    public <T> T getParam(String name, Class<T> targetType, T defaultValue) {
        return def(getParam(name, targetType), defaultValue);
    }

    @Override
    public <T> T getRequiredParam(String name, Class<T> targetType) throws IllegalStateException {
        return required(name, getParam(name, targetType));
    }

    @Override
    public String[] getParamNames() {
        return params.keySet().toArray(new String[params.size()]);
    }

    private static <T> T def(T value, T defaultValue) {
        return value == null ? defaultValue : value;
    }

    private static <T> T required(String name, T value) {
        if (value == null) {
            throw new IllegalStateException("required name [" + name + "] not found");
        }
        return value;
    }
}
