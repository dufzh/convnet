package net.convnet.server.protocol;

import net.convnet.server.ex.ConvnetException;
import net.convnet.server.protocol.bin.BinaryProtocol;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-13
 */
public final class DefaultProtocolFactory implements ProtocolFactory {
    private final Map<Integer, Protocol> protocols = new HashMap<Integer, Protocol>();
    private int defaultVersion = BinaryProtocol.VERSION;

    public void setDefaultVersion(int defaultVersion) {
        this.defaultVersion = defaultVersion;
    }

    public void setProtocols(List<Protocol> protocols) {
        for (Protocol protocol : protocols) {
            this.protocols.put(protocol.getVersion(), protocol);
        }
    }

    @Override
    public Protocol getProtocol(int version) {
        Protocol protocol = protocols.get(version);
        if (protocol == null) {
            throw new ConvnetException("Protocol impl for version " + version + " not found");
        }
        return protocol;
    }

    @Override
    public Protocol getDefaultProtocol() {
        return getProtocol(defaultVersion);
    }
}
