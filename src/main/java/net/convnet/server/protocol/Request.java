package net.convnet.server.protocol;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-14
 */
public interface Request {

    int getVersion();

    Cmd getCmd();

    boolean hasAttr(String name);

    <T> T getAttr(String name);

    Request setAttr(String name, Object value);

    boolean hasParam(String name);

    String getParam(String name);

    String getParam(String name, String defaultValue);

    Object getRawParam(String name);

    String getRequiredParam(String name) throws IllegalStateException;

    String[] getParams(String name);

    Integer getIntParam(String name);

    int getRequiredIntParam(String name) throws IllegalStateException;

    int getIntParam(String name, int defaultValue);

    <T> T getParam(String name, Class<T> targetType);

    <T> T getParam(String name, Class<T> targetType, T defaultValue);

    <T> T getRequiredParam(String name, Class<T> targetType) throws IllegalStateException;

    String[] getParamNames();
}
