package net.convnet.server.protocol;

import java.util.Map;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-14
 */
public interface RequestBuilder {

    RequestBuilder set(String name, Object value);

    RequestBuilder set(Map<String, Object> map);
}
