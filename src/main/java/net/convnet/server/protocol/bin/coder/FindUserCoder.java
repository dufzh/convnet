package net.convnet.server.protocol.bin.coder;

import net.convnet.server.identity.User;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.RequestBuilder;
import net.convnet.server.protocol.ResponseReader;
import net.convnet.server.protocol.bin.AbstractPacketCoder;
import net.convnet.server.protocol.bin.BinaryPacket;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-12
 */
@Service
public class FindUserCoder extends AbstractPacketCoder {

    @Override
    public Cmd getCmd() {
        return Cmd.FIND_USER;
    }

    @Override
    public Cmd getRespCmd() {
        return Cmd.FIND_USER_RESP;
    }

    @Override
    public void decode(RequestBuilder builder, BinaryPacket packet) {
        builder.set("type", packet.get(0));
        builder.set("value", packet.get(1));
    }

    @Override
    public void encode(ResponseReader reader, BinaryPacket packet) {
        List<User> users = reader.getAttr("users");
        if (users==null)
        {
            packet.add("N");
            return;
        }
        packet.add(users.size());
        for (User user : users) {
            packet.add(user.getId());
            packet.add(user.getName());
            packet.add(user.getNickName());
        }
        super.encode(reader, packet);
    }
}
