package net.convnet.server.protocol.bin;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufProcessor;
import net.convnet.server.Constants;
import net.convnet.server.ex.CodecException;
import net.convnet.server.ex.ConvnetException;
import net.convnet.server.ex.ErrorCode;
import net.convnet.server.ex.ExceptionUtils;
import net.convnet.server.protocol.*;
import net.convnet.server.util.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.dao.DataAccessException;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-12
 */
public final class BinaryProtocol implements Protocol, ApplicationContextAware, InitializingBean {
    public static final int VERSION = 1;
    public static final String VERSION_CODE = "4.2";
    public static final char SEPARATOR = ',';
    public static final char STAR = '*';
    private Map<Cmd, PacketCoder> packetCoders = new HashMap<Cmd, PacketCoder>();
    private List<CoderMapping> coderMappings = Collections.emptyList();
    private ApplicationContext appCtx;

    public void setCoderMappings(List<CoderMapping> coderMappings) {
        this.coderMappings = coderMappings;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    @Override
    public String getVersionCode() {
        return VERSION_CODE;
    }

    @Override
    public Request decode(ByteBuf buff) {
        BinaryPacket packet = parseBinaryPacket(buff);
        DefaultRequest request = new DefaultRequest(packet.getCmd(), getVersion());
        getCoder(packet.getCmd()).decode(request, packet);
        return request;
    }

    private BinaryPacket parseBinaryPacket(ByteBuf buff) {
        byte c = buff.getByte(0);
        if (c == '0') {//如果第一个字节是0.表示二进制传输
            int index = buff.forEachByte(new ByteBufProcessor() {
                @Override
                public boolean process(byte value) throws Exception {
                    return value != '*';
                }
            });
            String[] arr = StringUtils.splitPreserveAllTokens(buff.toString(2, index - 2, Constants.CHARSET), SEPARATOR);
            return new BinaryPacket(Cmd.SERVER_TRANS, new Object[]{arr[1], buff});
        } else {
            String body = buff.toString(Constants.CHARSET);
            String[] dataArr;
            Cmd cmd;
            int starIndex = body.indexOf(STAR);
            if (starIndex > -1) {
                String[] arr = StringUtils.splitPreserveAllTokens(body.substring(0, starIndex), SEPARATOR);
                dataArr = new String[arr.length];
                System.arraycopy(arr, 1, dataArr, 0, dataArr.length - 1);
                dataArr[dataArr.length - 1] = body.substring(starIndex + 1);
                cmd = toCmd(arr[0]);
            } else {
                String[] arr = StringUtils.splitPreserveAllTokens(body, SEPARATOR);
                dataArr = new String[arr.length - 1];
                System.arraycopy(arr, 1, dataArr, 0, dataArr.length);
                cmd = toCmd(arr[0]);
            }
            return new BinaryPacket(cmd, dataArr);
        }
    }

    @Override
    public void encode(ResponseReader reader, ByteBuf buff) {
        if (!reader.needOutput()) {
            return;
        }
        Cmd cmd = reader.getCmd();
        if (cmd == Cmd.SERVER_TRANS) {
            buff.writeBytes((ByteBuf) reader.getAttr("payload"));
            return;
        }
        BinaryPacket packet = new BinaryPacket();
        if (reader.isSuccess() || cmd == Cmd.ERROR) {
            getCoder(cmd).encode(reader, packet);
        }
        StringBuilder sb = new StringBuilder(32);
        sb.append(cmd.toOrdinal());

        //if (Cmd.RETURN_BOOLEAN_CMDS.contains(cmd)) {
        //    sb.append(SEPARATOR).append(reader.isSuccess() ? 'T' : 'F');
        //}
        for (Object part : packet.getParts()) {
            sb.append(SEPARATOR);
            if (part instanceof Boolean) {
                sb.append((Boolean) part ? 'T' : 'F');
            } else if (part != null) {
                sb.append(part.toString());
            }
        }
        IOUtils.writeString(buff, sb.toString());
    }

    @Override
    public Response createResponse(Cmd cmd) {
        return new DefaultResponse(getVersion(), getCoder(cmd).getRespCmd());
    }

    @Override
    public Response exToResponse(Throwable e) {
        Response response = createResponse(Cmd.ERROR);
        if (e instanceof InvocationTargetException) {
            e = ((InvocationTargetException) e).getTargetException();
        }
        int code = ErrorCode.ILLEGAL_PARAM;
        if (e instanceof ConvnetException) {
            code = ((ConvnetException) e).getCode();
        } else if (e instanceof IllegalArgumentException) {
            code = ErrorCode.ILLEGAL_PARAM;
        } else if (e instanceof IllegalStateException) {
            code = ErrorCode.ILLEGAL_STATE;
        } else if (e instanceof UnsupportedOperationException) {
            code = ErrorCode.UN_SUPPORTED;
        } else if (e instanceof DataAccessException || e instanceof HibernateException) {
            code = ErrorCode.ENTITY_ERROR;
        }
        response.setAttr("code", code);
        response.setAttr("msg", ExceptionUtils.buildMessage(e));
        return response;
    }

    private PacketCoder getCoder(Cmd cmd) {
        PacketCoder coder = packetCoders.get(cmd);
        if (coder == null) {
            throw new CodecException("PacketCoder for cmd [" + cmd + "] not found");
        }
        return coder;
    }

    private Cmd toCmd(String s) {
        try {
            return Cmd.values()[Integer.parseInt(s)];
        } catch (NumberFormatException e) {
            throw new CodecException("Invalid cmd [" + s + "]");
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.appCtx = applicationContext;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        for (PacketCoder coder : appCtx.getBeansOfType(PacketCoder.class).values()) {
            packetCoders.put(coder.getCmd(), coder);
            packetCoders.put(coder.getRespCmd(), coder);
        }
        for (CoderMapping mapping : coderMappings) {
            packetCoders.put(mapping.getCmd(), mapping);
            packetCoders.put(mapping.getRespCmd(), mapping);
        }
    }
}
