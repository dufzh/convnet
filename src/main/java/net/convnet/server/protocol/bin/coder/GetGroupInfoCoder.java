package net.convnet.server.protocol.bin.coder;

import net.convnet.server.identity.Group;
import net.convnet.server.identity.User;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.ResponseReader;
import net.convnet.server.protocol.bin.AbstractPacketCoder;
import net.convnet.server.protocol.bin.BinaryPacket;
import net.convnet.server.session.SessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-12
 */
@Service
public class GetGroupInfoCoder extends AbstractPacketCoder {
    @Autowired
    private SessionManager sessionManager;

    @Override
    public Cmd getCmd() {
        return Cmd.GET_GROUP_INFO;
    }

    @Override
    public Cmd getRespCmd() {
        return Cmd.GET_GROUP_INFO_RESP;
    }

    @Override
    public void encode(ResponseReader reader, BinaryPacket packet) {
        List<Group> groups = reader.getAttr("groups");
        for (Group group : groups) {
            addGroup(packet, group);
        }
        super.encode(reader, packet);
    }

    private void addGroup(BinaryPacket packet, Group group) {
        packet.add("G");
        packet.add(group.getName());
        packet.add(group.getId());
        packet.add(group.getCreator().getId());
        for (User user : group.getUsers()) {
            addUser(packet, user);
        }
    }

    private void addUser(BinaryPacket packet, User user) {
        packet.add("U");
        packet.add(user.getNickName());
        packet.add(user.getId());
        packet.add(sessionManager.getSession(user.getId())!=null);
    }
}
