package net.convnet.server.protocol.bin.coder;

import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.P2PCallType;
import net.convnet.server.protocol.RequestBuilder;
import net.convnet.server.protocol.ResponseReader;
import net.convnet.server.protocol.bin.AbstractPacketCoder;
import net.convnet.server.protocol.bin.BinaryPacket;
import org.springframework.stereotype.Service;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-27
 */
@Service
public class CallToUserCoder extends AbstractPacketCoder {
    @Override
    public Cmd getCmd() {
        return Cmd.CALL_TO_USER;
    }

    @Override
    public Cmd getRespCmd() {
        return Cmd.CALL_TO_USER_RESP;
    }
    //<bean parent="cm" p:cmd="CALL_TO_USER" p:rCmd="CALL_TO_USER_RESP" p:decode="id,count,password" p:encode="p2phead,id,mac"/>
    @Override
    public void decode(RequestBuilder builder, BinaryPacket packet) {
        builder.set("id", packet.get(0));
        builder.set("count", packet.get(1));
        builder.set("password", packet.get(2));
    }

    @Override
    public void encode(ResponseReader reader, BinaryPacket packet) {
        P2PCallType callType=reader.getAttr("callType");
        packet.add(callType.ordinal());
        switch (callType) {
            case ALL_DATA:

                break;
            case UDP_S2S:
                packet.add(reader.getAttr("callerid"));
                packet.add(reader.getAttr("callerip"));
                packet.add(reader.getAttr("callerudpport"));
                packet.add(reader.getAttr("callermac"));
                break;
            case UDP_S2SResp:
                break;
            case UDP_C2S:
                packet.add(reader.getAttr("callerid"));
                packet.add(reader.getAttr("callerip"));
                packet.add(reader.getAttr("callerudpport"));
                packet.add(reader.getAttr("callermac"));
                break;
            case UDP_C2SResp:
                break;
            case UDP_C2C:
                packet.add(reader.getAttr("callerid"));
                packet.add(reader.getAttr("callerip"));
                packet.add(reader.getAttr("callerudpport"));
                packet.add(reader.getAttr("callermac"));
                break;
            case UDP_C2CResp:
                break;
            case UDP_GETPORT:
                packet.add(reader.getAttr("callerid"));
                packet.add(reader.getAttr("callermac"));
                break;
            case UDP_P2PResp:
                break;
            case TCP_C2S:
                packet.add(reader.getAttr("callerid"));
                packet.add(reader.getAttr("callerip"));
                packet.add(reader.getAttr("callertcpport"));
                packet.add(reader.getAttr("callermac"));
                break;
            case TCP_C2SResp:
                break;
            case TCP_SvrTrans:
                packet.add(reader.getAttr("id"));
                packet.add(reader.getAttr("mac"));
                break;
            case ALL_NOTARRIVE:
                break;
            case NOTCONNECT:
                break;
            case DISCONNECT:
                break;
            case SAMEIP_CALL:
                packet.add(reader.getAttr("id"));
                break;
        }
    }
}
