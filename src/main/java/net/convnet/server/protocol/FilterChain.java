package net.convnet.server.protocol;

import net.convnet.server.session.Session;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
public interface FilterChain {

    void doFilter(Session session, Request request, Response response);
}
