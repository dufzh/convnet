package net.convnet.server.protocol;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-7
 */


public enum P2PCallType{
    ALL_DATA,          //发送数据
    UDP_S2S,
    UDP_S2SResp,
    UDP_C2S,
    UDP_C2SResp,
    UDP_C2C,
    UDP_C2CResp,
    UDP_GETPORT,
    UDP_P2PResp,
    TCP_C2S,
    TCP_C2SResp,
    TCP_SvrTrans,
    ALL_NOTARRIVE,     //所有方法无法到达
    NOTCONNECT,        //对方无法连接
    DISCONNECT,        //断开连接
    SAMEIP_CALL        //相同IP连接
}
