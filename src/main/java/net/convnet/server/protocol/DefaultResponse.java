package net.convnet.server.protocol;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-14
 */
public final class DefaultResponse implements Response, Serializable {
    private static final long serialVersionUID = 3092376853691143104L;
    private final Map<String, Object> attrs = new HashMap<String, Object>();

    private final int version;
    private Cmd cmd;
    private boolean success = true;
    private boolean needOutput = true;

    public DefaultResponse(int version, Cmd cmd) {
        this.version = version;
        this.cmd = cmd;
    }

    @Override
    public int getVersion() {
        return version;
    }

    @Override
    public Cmd getCmd() {
        return cmd;
    }

    @Override
    public void setCmd(Cmd cmd) {
        this.cmd = cmd;
    }

    public boolean isSuccess() {
        return success;
    }

    @Override
    public boolean needOutput() {
        return needOutput;
    }

    @Override
    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public void setOutput(boolean needOutput) {
        this.needOutput = needOutput;
    }

    @Override
    public Response setAttr(String name, Object value) {
        attrs.put(name, value);
        return this;
    }

    @SuppressWarnings("unchecked")
    public <T> T getAttr(String name) {
        return (T) attrs.get(name);
    }

    public Map<String, Object> getAttrs() {
        return attrs;
    }

    @Override
    public Response setAttrs(Map<String, Object> attrs) {
        this.attrs.putAll(attrs);
        return this;
    }
}
