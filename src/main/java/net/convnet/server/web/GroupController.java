package net.convnet.server.web;

import net.convnet.server.identity.GroupManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-12-27
 */
@Controller
@RequestMapping(value = "group")
public class GroupController {

    @Autowired
    private GroupManager groupManager;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) throws Exception {
        model.addAttribute("groups", groupManager.getAllGroup());
        return "group/index";
    }
    @RequestMapping(value = "delete", method = RequestMethod.GET)
    public String delete(@RequestParam(value = "id") int id) throws Exception {
        groupManager.removeGroup(id);
        return "redirect:/group";
    }
}
