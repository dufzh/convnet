package net.convnet.server.web;


import com.google.common.collect.Maps;
import net.convnet.server.identity.User;
import net.convnet.server.identity.UserManager;
import net.convnet.server.session.Session;
import net.convnet.server.session.SessionManager;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import net.convnet.server.protocol.Response;
import net.convnet.server.protocol.*;

import java.util.Map;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-12-27
 */
@Controller
@RequestMapping(value = "user")
public class UserController extends BaseController {

    @Autowired
    private UserManager userManager;

    @Autowired
    private SessionManager sessionManager;
    
    @ModelAttribute("user")
    public User getFile(@RequestParam(value = "id", required = false) Integer id) throws Exception {
        return id == null ? new User() : userManager.getUser(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model,
                        @RequestParam(value = "isonline", required = false) Boolean isonline,
                        @RequestParam(value = "name", required = false) String name,
                        Pageable request) throws Exception {

        if (isonline==null)
        {isonline=false;}
        Page<User> page= userManager.findUser(name, request, isonline);

        //Map<Integer,Boolean> statusMap= Maps.newHashMap();
        //for(User user:page){
        //    statusMap.put(user.getId(),sessionManager.isOnline(user.getId()));
        //}
        model.addAttribute("name",name);
        model.addAttribute("isonline",isonline);
        model.addAttribute("page", page);
        //model.addAttribute("statusMap", statusMap);
        return "user/index";
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET)
    public String delete(@RequestParam(value = "id") int id) throws Exception {
        userManager.removeUser(id);
        return "redirect:/user";
    }

    @RequestMapping(value = "edit", method = RequestMethod.GET)
    public String edit() throws Exception {
        return "user/edit";
    }

    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public String save(@ModelAttribute("user") User user, RedirectAttributes ra) throws Exception {
        try {
            if (StringUtils.isNotBlank(user.getPassword())) {
                userManager.setPassword(user, user.getPassword());
            }
            userManager.saveUser(user);
        } catch (Exception e) {
            failed(ra, e.getMessage());
        }
        success(ra);
        return "redirect:/user";
    }

    @RequestMapping(value = "sendmessage", method = RequestMethod.GET)
    public String sendmessage( Model model,@RequestParam(value = "id") int id,
                             RedirectAttributes ra) throws Exception {
        model.addAttribute("user",userManager.getUser(id));
        return "user/sendmessage";
    }

    @RequestMapping(value = "serverban", method = RequestMethod.GET)
    public String serverban( Model model,@RequestParam(value = "id") int id,
                               RedirectAttributes ra) throws Exception {
        Session session= sessionManager.getSession(id);
        if (session!=null)
        {
            session.destory();
        }
        return "redirect:/user";
    }


    @RequestMapping(value = "sendmessagetouser", method = RequestMethod.POST)
    public String sendmessagetouser(Model model,@RequestParam(value = "id") int id,
                                    @RequestParam(value = "message") String message,
                              RedirectAttributes ra) throws Exception {
        model.addAttribute("id",id);
        if (sessionManager.sendMessageToUser(userManager.getUser(id),message))
            model.addAttribute("msg",message+"发送成功");
        else
            model.addAttribute("msg",message+"发送失败");
        return "user/sendmessage";
    }
}
