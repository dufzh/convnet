package net.convnet.server.email;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.activation.DataSource;
import java.util.Date;
import java.util.List;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 12-8-28
 */
public class Email {

    public static interface Callback {

        void error(Email email);

        void complete(Email email);
    }

    private String from;
    private String fromLabel;
    private String[] to;
    private String[] cc;
    private String[] bcc;
    private String replyTo;
    private Date sentDate;
    private int priority;
    private String subject;
    private String body;
    private List<DataSource> attachment;
    private List<DataSource> inline;
    private Callback callback;
    private String feedback;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String[] getTo() {
        return to;
    }

    public void setTo(String[] to) {
        this.to = to;
    }

    public String[] getCc() {
        return cc;
    }

    public void setCc(String[] cc) {
        this.cc = cc;
    }

    public String[] getBcc() {
        return bcc;
    }

    public void setBcc(String[] bcc) {
        this.bcc = bcc;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public List<DataSource> getAttachment() {
        return attachment;
    }

    public void setAttachment(List<DataSource> attachment) {
        this.attachment = attachment;
    }

    public List<DataSource> getInline() {
        return inline;
    }

    public void setInline(List<DataSource> inline) {
        this.inline = inline;
    }

    public Callback getCallback() {
        return callback;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getFromLabel() {
        return fromLabel;
    }

    public void setFromLabel(String fromLabel) {
        this.fromLabel = fromLabel;
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
