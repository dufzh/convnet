package net.convnet.server.core;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import net.convnet.server.ex.ConvnetException;
import net.convnet.server.protocol.Cmd;
import net.convnet.server.protocol.Request;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.orm.hibernate4.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.util.concurrent.Callable;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-15
 */
@ChannelHandler.Sharable
public final class HibernateSessionHandle extends ChannelInboundHandlerAdapter {
    private static final Logger LOG = LoggerFactory.getLogger(HibernateSessionHandle.class);
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void channelUnregistered(final ChannelHandlerContext ctx) throws Exception {
        callWithSession(new Callable() {
            @Override
            public Object call() throws Exception {
                ctx.fireChannelUnregistered();
                return null;
            }
        });
    }

    @Override
    public void channelRead(final ChannelHandlerContext ctx, final Object msg) throws Exception {
        Request request = (Request) msg;
        if (request.getCmd() == Cmd.SERVER_TRANS || request.getCmd() == Cmd.KEEP_ONLINE) {
            ctx.fireChannelRead(msg);
            return;
        }
        callWithSession(new Callable() {
            @Override
            public Object call() throws Exception {
                ctx.fireChannelRead(msg);
                return null;
            }
        });
    }

    private void callWithSession(Callable callable) throws Exception {
        boolean participate = false;
        if (TransactionSynchronizationManager.hasResource(sessionFactory)) {
            participate = true;
        } else {
            LOG.trace("Opening Hibernate Session in OpenSessionInViewFilter");
            Session session = openSession(sessionFactory);
            SessionHolder sessionHolder = new SessionHolder(session);
            TransactionSynchronizationManager.bindResource(sessionFactory, sessionHolder);
        }
        try {
            callable.call();
        } finally {
            if (!participate) {
                SessionHolder sessionHolder = (SessionHolder) TransactionSynchronizationManager.unbindResource(sessionFactory);
                LOG.trace("Closing Hibernate Session in OpenSessionInViewFilter");
                SessionFactoryUtils.closeSession(sessionHolder.getSession());
            }
        }
    }

    private Session openSession(SessionFactory sessionFactory) {
        try {
            Session session = sessionFactory.openSession();
            session.setFlushMode(FlushMode.MANUAL);
            return session;
        } catch (HibernateException ex) {
            throw new ConvnetException("Could not open Hibernate Session", ex);
        }
    }
}
