package net.convnet.server.core;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import net.convnet.server.protocol.*;
import net.convnet.server.session.Session;
import net.convnet.server.session.SessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
final class ConvnetHandler extends ChannelInboundHandlerAdapter {
    private static final Logger LOG = LoggerFactory.getLogger(ConvnetHandler.class);
    private final ProtocolFactory protocolFactory;
    private final SessionManager sessionManager;
    private final FilterChain filterChain;

    ConvnetHandler(ProtocolFactory protocolFactory, SessionManager sessionManager, FilterChain filterChain) {
        this.protocolFactory = protocolFactory;
        this.sessionManager = sessionManager;
        this.filterChain = filterChain;
    }

    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
        sessionManager.createAnonymousSession(ctx.channel());
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        Session session = sessionManager.getSession(ctx.channel());
        if (session != null) {
            session.destory();
        }
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        Request request = (Request) msg;
        Cmd cmd = request.getCmd();
        if (cmd == Cmd.ERROR) {
            ctx.write(request);
            return;
        }
        Session session = sessionManager.getSession(ctx.channel());
        if (session == null || !session.isLogin() && !Cmd.ANONYMOUS_CMDS.contains(cmd)) {
            //throw new ConvnetException(ErrorCode.NO_PERMISSON, "Not logined");
            return;
        }
        Protocol protocol = protocolFactory.getProtocol(request.getVersion());
        Response response = protocol.createResponse(cmd);
        try {
            filterChain.doFilter(session, request, response);
        } catch (Throwable e) {
            if (LOG.isInfoEnabled()) {
                LOG.error("Process cmd [" + cmd + "] error", e);
            }
            response.setSuccess(false);
        }
        if (response.needOutput()) {
            ctx.write(response);
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable e) throws Exception {
       // Protocol protocol = sessionManager.getProtocol(ctx.channel());
        if (LOG.isInfoEnabled()) {
            LOG.error("Serve error", e);
        }
        //ctx.write(protocol.exToResponse(e));
    }
}
