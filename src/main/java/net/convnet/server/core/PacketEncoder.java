package net.convnet.server.core;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import net.convnet.server.Constants;
import net.convnet.server.protocol.ProtocolFactory;
import net.convnet.server.protocol.ResponseReader;
import org.apache.log4j.helpers.LogLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
@ChannelHandler.Sharable
final class PacketEncoder extends MessageToByteEncoder<ResponseReader> {
    private static final Logger LOG = LoggerFactory.getLogger(PacketEncoder.class);
    private final ProtocolFactory protocolFactory;

    PacketEncoder(ProtocolFactory protocolFactory) {
        this.protocolFactory = protocolFactory;
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, ResponseReader reader, ByteBuf out) throws Exception {
        int start = out.writerIndex();
        out.writeInt(0);
        protocolFactory.getProtocol(reader.getVersion()).encode(reader, out);
        int len = out.readableBytes() - 4;
        out.markWriterIndex();
        out.writerIndex(start);
        out.writeInt(len);
        out.resetWriterIndex();
        if (LOG.isDebugEnabled()) {
            LOG.debug("Send packet: [" + out.toString(Constants.CHARSET) + "]");
        }
        Long writeBytes = ctx.channel().attr(Constants.WRITE_BYTES_KEY).get();
        if (writeBytes == null) {
            writeBytes = 0l;
        }
        ctx.channel().attr(Constants.WRITE_BYTES_KEY).set(writeBytes + len);

    }
}
