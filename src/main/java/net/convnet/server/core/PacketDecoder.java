package net.convnet.server.core;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.TooLongFrameException;
import net.convnet.server.Constants;
import net.convnet.server.session.SessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-9
 */
final class PacketDecoder extends LengthFieldBasedFrameDecoder {
    private static final Logger LOG = LoggerFactory.getLogger(PacketDecoder.class);
    private SessionManager sessionManager;

    PacketDecoder(SessionManager sessionManager) {
        super(50 * 1024, 0, 4, 0, 4);
        this.sessionManager = sessionManager;
    }

    @Override
    protected Object decode(ChannelHandlerContext ctx, ByteBuf in) throws Exception {
        ByteBuf frame;
        try {
            frame = (ByteBuf) super.decode(ctx, in);
        } catch (Exception e) {
            LOG.info("Frame decode error", e);
            ctx.channel().close();
            return null;
        }

        Long readBytes = ctx.channel().attr(Constants.READ_BYTES_KEY).get();
        if (readBytes == null) {
            readBytes = 0l;
        }

        if (frame == null) {
            return null;
        }

        ctx.channel().attr(Constants.READ_BYTES_KEY).set(readBytes + frame.readableBytes());

        if (LOG.isDebugEnabled()) {
            LOG.debug("Receive packet [" + frame.toString(Constants.CHARSET) + "]");
        }
        return sessionManager.getProtocol(ctx.channel()).decode(frame);
    }
}
