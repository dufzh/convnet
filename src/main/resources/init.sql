alter table cvn_user_ex add column send_to_server bigint not null default 0;
alter table cvn_user_ex add column recive_from_server bigint not null  default 0;
alter table cvn_user_ex add column user_is_online boolean not null default false;

alter table cvn_user add column can_addfriend boolean not null default true;
alter table cvn_user add column can_create_group boolean not null default true;
alter table cvn_user add column recive_limit bigint not null default 0;
alter table cvn_user add column send_limit bigint not null default 0;
update cvn_user_ex set user_is_online = false where user_is_online = true;
commit;