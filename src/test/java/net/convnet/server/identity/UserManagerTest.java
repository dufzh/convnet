package net.convnet.server.identity;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.orm.hibernate4.SessionHolder;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;

/**
 * .
 * <p/>
 *
 * @author <a href="mailto:oxsean@gmail.com">sean yang</a>
 * @version V1.0, 13-8-29
 */
@ContextConfiguration(locations = "classpath*:/applicationContext.xml")
public class UserManagerTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    private UserManager userManager;
    @Autowired
    private GroupManager groupManager;

    @Autowired
    protected SessionFactory sessionFactory;

    @Before
    public void setUp() {
        Session session = sessionFactory.openSession();
        session.setFlushMode(FlushMode.MANUAL);
        TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(session));
    }

    @After
    public void tearDown() {
        SessionFactoryUtils.closeSession(((SessionHolder) TransactionSynchronizationManager.unbindResource(sessionFactory)).getSession());
    }

    @Test
    public void testSaveUser() throws Exception {

        File file = new File("d://1.txt");
        BufferedReader reader = null;
        try {
            System.out.println("以行为单位读取文件内容，一次读一整行：");
            reader = new BufferedReader(new FileReader(file));
            String tempString = null;
            int line = 0;
            // 一次读入一行，直到读入null为文件结束
            while ((tempString = reader.readLine()) != null) {
                // 显示行号
                User user = new User();
                if (tempString == "336") {
                    System.out.print(tempString);
                }
                user.setId(Integer.getInteger(tempString));
                user.setNickName(reader.readLine());
                user.setName(reader.readLine());
                user.setDescription(reader.readLine());
                user.setPassword(reader.readLine());
                user.setAllowpass1(reader.readLine());
                user.setAllowpass2(reader.readLine());
                user.setFriendpass(reader.readLine());
                user.setDospass(reader.readLine());
                user.setRegisterIp("127.0.0.1");
                userManager.saveUser(user);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }
    }

    @Rollback(false)
    @Test
    public void testSavePeers() throws Exception {
        File file = new File("d://2.txt");
        BufferedReader reader = null;
        try {
            System.out.println("以行为单位读取文件内容，一次读一整行：");
            reader = new BufferedReader(new FileReader(file));
            String tempString = null;
            int line = 0;
            // 一次读入一行，直到读入null为文件结束
            while ((tempString = reader.readLine()) != null) {
                // 显示行号
                int userid = Integer.parseInt(tempString);
                int tarid = Integer.parseInt(reader.readLine());
                User user1 = userManager.getUser(userid);
                User user2 = userManager.getUser(tarid);
                user1.getFriends().add(user2);
                user2.getFriends().add(user1);
                userManager.saveUser(user1);
                userManager.saveUser(user2);

            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }

    }

    @Rollback(true)
    @Test
    public void testSaveGroupInfo() throws Exception {
        File file = new File("d://3.txt");
        BufferedReader reader = null;
        try {
            System.out.println("以行为单位读取文件内容，一次读一整行：");
            /*
            stringlist.add(SQLQuery1.FieldByName('GROUPID').AsString);
            stringlist.add(SQLQuery1.FieldByName('GROUPNAME').AsString);
            stringlist.add(SQLQuery1.FieldByName('GROUPDESC').AsString);
            stringlist.add(SQLQuery1.FieldByName('CREATOR').AsString);
            stringlist.add(SQLQuery1.FieldByName('GROUPPASS').AsString);
            */

            reader = new BufferedReader(new FileReader(file));
            String tempString = null;
            int line = 0;
            // 一次读入一行，直到读入null为文件结束
            while ((tempString = reader.readLine()) != null) {
                // 显示行号
                Group group = new Group();
                group.setId(Integer.parseInt(tempString));
                String groupname = reader.readLine();

                Group checker=groupManager.getGroupByName(groupname);
                if (checker!=null)
                {
                    groupname=groupname+"Re";
                }

                group.setName(groupname);
                String groupdesc = reader.readLine();
                group.setDescription(groupdesc);

                int creatorid = Integer.parseInt(reader.readLine());
                User user = userManager.getUser(creatorid);
                group.setCreator(user);
                group.setAdmins(Collections.singleton(user));
                group.setPassword(reader.readLine());
                group.setCreateAt(new Date());
                groupManager.saveGroup(group);
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }

    }


    @Rollback(false)
    @Test
    public void testSaveGroupUser() throws Exception {
        File file = new File("d://4.txt");
        BufferedReader reader = null;
        try {
            System.out.println("以行为单位读取文件内容，一次读一整行：");
            reader = new BufferedReader(new FileReader(file));
            String tempString = null;
            int line = 0;
            // 一次读入一行，直到读入null为文件结束
            while ((tempString = reader.readLine()) != null) {
                // 显示行号
                int groupid = Integer.parseInt(tempString);
                int userid = Integer.parseInt(reader.readLine());
                Group group = groupManager.getGroup(groupid);
                User user = userManager.getUser(userid);
                group.getUsers().add(user);
                groupManager.saveGroup(group);
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }

    }


}
